﻿using Manager;
using UnityEngine;
using Spaceship;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        //[SerializeField] private PlayerSpaceship playerSpaceship;

        private PlayerSpaceship spawnedPlayerShip;
        
        void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            // TODO: Implement this later

            spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;

            if (spawnedPlayerShip == null)
            {
                return;
            }
            
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            Debug.Log(distanceToPlayer);

            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2) (spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
        }
    }    
}


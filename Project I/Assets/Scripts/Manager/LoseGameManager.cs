﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class LoseGameManager : MonoBehaviour
    {
        [SerializeField] private Button QuitButtonLose;
        [SerializeField] private RectTransform dislog;
        [SerializeField] private TextMeshProUGUI SummaryScoreTextLose;

        private void Awake()
        {
            Debug.Assert(QuitButtonLose != null, "QuitButtonLose cannot be null");
            Debug.Assert(dislog != null, "dislog cannot be null");
            Debug.Assert(SummaryScoreTextLose != null, "SummaryScoreTextLose cannot be null");
            
            QuitButtonLose.onClick.AddListener(OnQuittButtonLoseClicked);
            
            SummaryScoreTextLose.text = $"Summary Score : {ScoreManager.Instance.SummaryScore}";
        }
        
        private void OnQuittButtonLoseClicked()
        {
            Application.Quit();
        }
    }
}

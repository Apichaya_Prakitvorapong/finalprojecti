﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Spaceship;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI ScoreText;
        [SerializeField] private TextMeshProUGUI SummaryScoreText;
        
        public int SummaryScore;
        public int score;
        
        public static  ScoreManager Instance { get; private set; }
        
       public void Init()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            score = 0;
            SetScore();
            return;
        }
    
        public void SetScore()
        {
            ScoreText.text = $"Score : {score}";
            SummaryScore = score;
            SummaryScoreText.text = $"Summary Score : {SummaryScore}";
        }
    
        private void Awake()
        {
            Debug.Assert(ScoreText != null, "ScoreText cannot be null");
            Debug.Assert(SummaryScoreText != null, "SummaryScoreText cannot be null");
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(ScoreText);
            DontDestroyOnLoad(SummaryScoreText);
        }
    
        public void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            score = 0;
            SetScore();
            return;
        }
        
        private void HideScore(bool hide)
        {
            //ScoreText.gameObject.SetActive(!hide);
        }
    }
}

﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using static Manager.SoundManager;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button StartButton;
        [SerializeField] private Button RestartButton;
        [SerializeField] private Button QuitButton;
        [SerializeField] private RectTransform dislog;
        [SerializeField] private RectTransform dislog2;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private Bonus bonusHP;

        public event Action OnRestarted;
        [SerializeField] public int playerSpaceshipHP;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] public int enemySpaceshipHP;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        public static  GameManager Instance { get; private set; }
        
        //SpawnShip
        public PlayerSpaceship spawnedPlayerShip;
        public EnemySpaceship spawnedEnemyShip;
        private float spawnenemyTime = 3f;
        public Transform[] spawnenemyPoints;
        private int spawnenemyPointIndex;

        //SpawnBonus
        public float spawnbonusTime = 5f;
        public Bonus spawnedbonusHp;
        public Transform[] spawnBousHpPoints;
        private int spawnBonusHpPointIndex;
        
        //HP Bar
        public HpBar healthBar;
        public int currencyHealth;

        private void Awake()
        {
            Debug.Assert(StartButton != null, "StartButton cannot be null");
            Debug.Assert(RestartButton != null, "RestartButton cannot be null");
            Debug.Assert(QuitButton != null, "QuitButton cannot be null");
            Debug.Assert(dislog != null, "dislog cannot be null");
            Debug.Assert(dislog2 != null, "dislog2 cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(bonusHP != null, "bonusHP cannot be null");
            Debug.Assert(playerSpaceshipHP > 0, "playerSpaceship HP has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeedP has to be more than zero");
            Debug.Assert(enemySpaceshipHP > 0, "enemySpaceship HP has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            
            RestartButton.onClick.AddListener(OnRestartButtonClicked);
            QuitButton.onClick.AddListener(OnQuittButtonClicked);
            StartButton.onClick.AddListener(OnStartButtonClicked);
            
            SoundManager.Instance.PlayBGM();
        }
        
        void Start()
        {
            InvokeRepeating("SpawnEnemySpaceship", spawnenemyTime, spawnenemyTime);
            InvokeRepeating("SpawnBonusHp", spawnbonusTime, spawnbonusTime);
        }

        public void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();

            dislog2.gameObject.SetActive(false);
        }

        // Ship
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHP, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceShipExploded;
        }

        private void OnPlayerSpaceShipExploded()
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        }

        public void SpawnEnemySpaceship()
        {
            spawnenemyPointIndex = Random.Range(0, spawnenemyPoints.Length);
            spawnedEnemyShip = Instantiate(enemySpaceship, spawnenemyPoints[spawnenemyPointIndex].position, spawnenemyPoints[spawnenemyPointIndex].rotation);
            spawnedEnemyShip.Init(enemySpaceshipHP, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceShipExploded;
        }
        
        public void OnEnemySpaceShipExploded()
        {
            ScoreManager.Instance.score += 1;
            ScoreManager.Instance.SetScore();
            //DestroyRemainingShips();
            
            SpawnEnemySpaceship();
            SpawnBonusHp();
        }
        
        //Bonus
        private void SpawnBonusHp()
        {
            spawnBonusHpPointIndex = Random.Range(0, spawnBousHpPoints.Length);
            spawnedbonusHp = Instantiate(bonusHP,spawnBousHpPoints[spawnBonusHpPointIndex].position, spawnBousHpPoints[spawnBonusHpPointIndex].rotation);
        }
        
        //Button
        public void OnStartButtonClicked()
        {
            dislog.gameObject.SetActive(false);
            StartGame();
        }
        
        private void OnRestartButtonClicked()
        {
            //SceneManager.LoadScene("Game");
            Restart();
        }
        
        private void OnQuittButtonClicked()
        {
            Application.Quit();
        }

        //DestroyObject
        public void Restart()
        {
            DestroyRemainingShips();
            DestroyPlayerShip();
            //dislog.gameObject.SetActive(true);
            ScoreManager.Instance.OnRestarted();
            if (OnRestarted != null) OnRestarted.Invoke();
        }
        
        private void DestroyRemainingShips()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }

            var remainingBonusHP = GameObject.FindGameObjectsWithTag("BonusHP");
            foreach (var bonusHp in remainingBonusHP)
            {
                Destroy(bonusHp);
            }
        }

        private void DestroyPlayerShip()
        {
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
    }
}

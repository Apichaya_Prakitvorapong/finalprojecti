﻿using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaulBullet;
        [SerializeField] protected Transform gunPosition;

        [SerializeField] protected AudioSource audioSource;

        public static int HP { get; set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }
    
        protected void Init(int hp, float speed, Bullet bullet)
        {
            HP = hp;
            Speed = speed;
            Bullet = bullet;
        }
        public abstract void Fire();
    }

}
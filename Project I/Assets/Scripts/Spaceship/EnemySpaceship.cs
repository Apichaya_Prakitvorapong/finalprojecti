﻿using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        // [SerializeField] private float enemyFireSoundVolume = 0.2f;
        [SerializeField] private float enemyDieSoundVolune = 0.5f;
        [SerializeField] private AudioClip enemyDieSound;
        
        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;

        private void Awake()
        {
            // Debug.Assert(enemyFireSound != null, "enemyFireSound cannot be null");
            Debug.Assert(enemyDieSound != null, "enemyDieSound cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaulBullet);
        }

        public void TakeHit(int damage)
        {
            HP -= damage;
            if (HP > 0)
            {
                return;
            }
            Exploded();
        }
        
        public void Exploded()
        {
            Debug.Assert(HP <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            
            AudioSource.PlayClipAtPoint(enemyDieSound,Camera.main.transform.position, enemyDieSoundVolune);
            //SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyDie);
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaulBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
                
                //AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyFireSoundVolume);
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyFire);
            }
        }
    }
}

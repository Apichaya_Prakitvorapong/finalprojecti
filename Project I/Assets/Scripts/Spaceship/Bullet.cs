﻿using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private new Rigidbody2D rigidbody2D;
        
        public int Damage
        {
            get => damage;
            set => damage = value;
        }
        
        public static Bullet Instance { get; private set; }

        public void Init(Vector2 direction)
        {
            Move(direction);
        }
    
        private void Awake()
        {
            Debug.Assert(rigidbody2D != null, "rigidbody2D cannot be null");
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

        private void Move(Vector2 direction)
        {
            rigidbody2D.velocity = direction * speed;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("ObjectDestroyer"))
            {
                Destroy(this.gameObject);
                return;
            }

            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
        }
    }
}
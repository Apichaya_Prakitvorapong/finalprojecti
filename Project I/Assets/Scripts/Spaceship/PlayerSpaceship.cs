﻿using System;
using System.Runtime.InteropServices;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private float playerDieSoundVolune = 0.5f;
        [SerializeField] private AudioClip playerDieSound;
        
        private void Awake()
        {
            Debug.Assert(defaulBullet != null, "defaulBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        private void Start()
        {
            GameManager.Instance.currencyHealth = GameManager.Instance.playerSpaceshipHP;
            GameManager.Instance.healthBar.SetMaxHealth(GameManager.Instance.playerSpaceshipHP);
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaulBullet);
        }
    
        public override void Fire()
        {
            var bullet = Instantiate(defaulBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
        }
        
    
        public void TakeHit(int damage)
        {
            GameManager.Instance.currencyHealth -= Bullet.Instance.Damage;
            GameManager.Instance.healthBar.SetHealth(GameManager.Instance.currencyHealth);
            if (GameManager.Instance.currencyHealth > 0)
            {
                return;
            }
            Exploded();
        }
    
        public void Exploded()
        {
            Debug.Assert(HP <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            
            AudioSource.PlayClipAtPoint(playerDieSound,Camera.main.transform.position, playerDieSoundVolune);
        }
    }
}

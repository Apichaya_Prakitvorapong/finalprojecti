﻿using UnityEngine;
using Manager;

public class Bonus : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            bonusHp();
        }
    }

    public void bonusHp()
    {
        Destroy(gameObject, 0.3f);
            
        if (gameObject.CompareTag("BonusHP"))
        {
            GameManager.Instance.currencyHealth = GameManager.Instance.currencyHealth + 20;
            GameManager.Instance.healthBar.SetHealth(GameManager.Instance.currencyHealth);
        }
    }
}

﻿using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;

        private float xMax;
        private float xMin;
        private float yMax;
        private float yMin;
        
        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }
        
        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }
        
        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.Fire();
        }
        
        public void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
               movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inPutVelocity = movementInput * playerSpaceship.Speed;
            var gravityForce = new Vector2(0, -0.5f);
            var finalVelocity = inPutVelocity + gravityForce;
            
            var newPosition = transform.position;
            newPosition.x = transform.position.x + finalVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + finalVelocity.y * Time.smoothDeltaTime;

            // Clamp movement within boundery
            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
            newPosition.y = Mathf.Clamp(newPosition.y, yMin, yMax);
            Debug.DrawRay(transform.position, inPutVelocity, Color.green);
            Debug.DrawRay(transform.position, gravityForce, Color.red);
            Debug.DrawRay(transform.position, finalVelocity, Color.white);
            
            transform.position = newPosition;
        }
        
        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "main Camera cannot be null");

            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null,"spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;
            xMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            xMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            yMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            yMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
